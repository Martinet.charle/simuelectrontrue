#include "MCfunctions1.h"
#define PTSTYLE 0
#define JTOEV 6.242e18
#define MASSEELECTRON 9.109e-31
#define CHARGEELEMENT 1.6e-19


#include "COMPoisson.h"

using namespace std;


/*!
    \brief Constructor
*/

SimuElectrons::SimuElectrons():
    m_meanPoisson(8)
{

    m_vElectrons = {};
}
///--------------------------///

/*!
    \brief Destructor
*/

SimuElectrons::~SimuElectrons(){

    m_vElectrons.clear();
}

///--------------------------///

// génére un nombre d'évenement dans le vecteur m_vElectrons avec un nombre de charge donnée (nelec : le nombre de charge primaire , nevement : le nombre d'évenement)
void SimuElectrons::LinearGenerate(int nelec, int nevenement){
    for(int j =0; j<nevenement; j++){
        //supress electrons
        ClearVectorElectrons();

        int nPrimary = 0 ;
		nPrimary = nelec;

		for(int i=0 ; i < nPrimary ; i++){
            SimuElectrons::Electrons tmpElec;
            tmpElec.nbrprimaire = nPrimary;
            tmpElec.x = 0.0;//cm
            tmpElec.y = 0.0;//cm
            tmpElec.t = 0.0;//s
            tmpElec.z = 400.0*(double(i)/nPrimary);//i*(50*m_vdrift);
            tmpElec.energy = 1.;//nbr electrons
            AddElectronToStack(tmpElec);
        }
            AddTraceTostack(m_vElectrons);

    }

}



// génére un nombre d'évenement dans le vecteur m_vElectrons selon un distribution compoisson (nevement : le nombre d'évenement)
void SimuElectrons::ComPoissonGenerate(int nevenement){
    for(int j =0; j<nevenement; j++){
        //supress electrons
        ClearVectorElectrons();

        //generate primary electron with poisson distribution for the moment
        int nPrimary = 0 ;


		nPrimary = GetRDMComPoisson();
		for(int i=0 ; i < nPrimary ; i++){
            SimuElectrons::Electrons tmpElec;       //linear
            tmpElec.nbrprimaire = nPrimary;
            tmpElec.x = 0.0;//cm
            tmpElec.y = 0.0;//cm
            tmpElec.t = 0.0;//s
            tmpElec.z = 400.0*(double(i)/nPrimary);
            tmpElec.energy = 1.;//nbr electron
            AddElectronToStack(tmpElec);
        }
            AddTraceTostack(m_vElectrons);

    }

}


// génére un nombre d'évenement dans le vecteur m_vElectrons a partir d'un fichier .dat casino
// probablement pas optimisé du tout
// peut étre amélioré en créant les événement un a un plutot que tous les prendre d'un coup
void SimuElectrons::CasioGenerate(char* file){


    string ctrajectory, cx, cy, cz, ce, backward, ccolision;
    vector<float> x;
    vector<float> y;
    vector<float> z;
    vector<float> e;
	ifstream casio(file); //opening the file.
	if (casio.is_open()) { //if the file is open

        string line ;
        getline(casio, line); //skip ------

        while ((!casio.eof())){

            ClearVectorElectrons();
            getline(casio, line,'	'); //skip space before trajectory
            getline(casio, line,'	'); //skip "trajecory"

            getline(casio, ctrajectory,'\n'); //give trajectory number


            getline(casio, line); //skip emptyline
            getline(casio, line); // skip name of some important thing (backward)

            getline(casio, backward,'	'); // take backwardness
            getline(casio, line,'	'); // skip unuse data
            getline(casio, line,'	'); // skip unuse data
            getline(casio, line,'	'); // skip unuse data
            getline(casio, ccolision,'	'); // get nbr colision


            getline(casio, line); //end line
            getline(casio, line); // skip line empti line
            getline(casio, line); // skip table name line


                x.clear();
                y.clear();
                z.clear();
                e.clear();
                for(int i = 0; i < stoi(ccolision)+1; i++){

                    getline(casio, cx,'	');
                    x.push_back(stof(cx)/(1000));

                    getline(casio, cy,'	');
                    y.push_back(stof(cy)/(1000));

                    getline(casio, cz,'	');
                    z.push_back(stof(cz)/(1000));

                    getline(casio, line,'	'); //skip unuse data
                    getline(casio, line,'	'); //skip unuse data
                    getline(casio, line,'	'); //skip unuse data

                    getline(casio, ce,'	');
                    e.push_back(stof(ce));

                    getline(casio, line);

                    if(i>0){
                        for(int j =0; j< int((e[i-1]-e[i])/0.04); j++){
                            Electrons tmpelec;
                            tmpelec.nbrprimaire=int((e[i-1]-e[i])/0.04);
                            tmpelec.x=x[i];
                            tmpelec.y=y[i];
                            tmpelec.z=z[i];
                            tmpelec.energy=e[i];
                            AddElectronToStack(tmpelec);
                        }
                    }

                }
                AddTraceTostack(m_vElectrons);



            getline(casio, line);
        }

    }

}


//la fonction de Manolis Dris and Theo Alexopoulos qui créé le courant (x la variable , par les different parrametre
/* par contient :
                 - (pour les i pair) : la position Z des éléctrons arrivant à la grille
                 - (pour les i impaire) : l'energie (le nombre d'électron aprés l'avalanche)
*/
double SimuElectrons::IonCurent(double *x, double *par){
    //initialisation des variable
    double xx=x[0];
    double pp=par[0];
    double pe=m_townsend;
    double elec=0.;
    double ion =0.;
    for(int i=0;i<20;i+=2){

    double beta = m_speedelectron/(m_speedelectron-m_speedion); // beta peut étre  remplacer par 1

        pp=par[i];
        pe=log(par[i+1])/m_ionlength;//m_townsend; // le nombre d'electrons réel ou un facteur de townsend constant
        if(par[i]==155){break;} //par est initialisé avec la valeur 155 qui est une valeur prise aléatoirement, si la valeur est 155 alors l'électron n'existe pas

        if(xx>=pp){ // cette maniére de faire implique que les éléctron d'un évenement sont trié par leur position Z croissante
            double elecbuf=(((CHARGEELEMENT*m_speedelectron)/m_ionlength)*(exp(pe*m_ionlength)-exp(pe*beta*m_speedelectron*(xx-pp))));//(1/(250*10e-12))*exp(-(xx/(250*10e-12))); //contribution ionique
            double ionbuf =(((CHARGEELEMENT*m_speedion)/m_ionlength)*(exp(pe*m_ionlength)-exp(pe*beta*m_speedion*(xx-pp)))); //contribution éléctrique

            //on ne prend en compte les contributtion des ions / electron que si elle sont positive (nécessaire car les contributions tendent vers - l'infinis)
            if(elecbuf>=0 && ionbuf>=0){
                elec+= elecbuf;
                ion += ionbuf;
            }
            if(elecbuf>=0 && ionbuf<=0){
                elec += elecbuf;
            }
            if(elecbuf<=0 && ionbuf>=0){
                ion +=ionbuf;
            }
            if(elec<=0 && ion<=0){
            }
        }
    }
    return elec +ion; // on retourne la contribution ionique et la contribution electronique

}


//poisson distribution
double SimuElectrons::PoissonDistribution(double *x, double *par){
   double xx =x[0];
   return exp(-m_meanPoisson) * pow(m_meanPoisson, xx) / TMath::Gamma(xx +1);
}


//gaussian distribution XY
double SimuElectrons::GaussianDistributionxy(double *x, double *par){
    double xx =x[0];
    return (1 / m_diffxy * sqrt(2*3.14)) * exp(-((pow(xx, 2))/(2*pow(m_diffxy,2))));

}

//Gaussian distribution Z
double SimuElectrons::GaussianDistributionz(double *x, double *par){
    double xx =x[0];
    return (1 / m_diffz * sqrt(2*3.14)) * exp(-((pow(xx, 2))/(2*pow(m_diffz,2))));

}

//polya distribution
double SimuElectrons::PolyaDistrib(double *x, double *par){
    double xx=x[0];
    return /*(1/m_meanPolya) * */  (pow(m_factorPolya + 1 , m_factorPolya + 1)/TMath::Gamma(m_factorPolya)) * pow(xx/m_meanPolya,m_factorPolya) * exp(-(m_factorPolya+1)*(xx/m_meanPolya));

}

//initialisation de tout les distribution qui seront stocké dans la class simulation
void SimuElectrons::CreateDistribs(int maxtrack){
    m_fpoisson = new TF1("fPoisson", this, &SimuElectrons::PoissonDistribution, 0, 100, 0);
    m_fpoisson->SetNpx(1e4);

    m_fgaussxy =  new TF1("fgaussxy", this, &SimuElectrons::GaussianDistributionxy, -6000, 6000, 0);
    m_fgaussxy->SetNpx(1e4);

    m_fgaussz =  new TF1("fgaussz", this, &SimuElectrons::GaussianDistributionz, -6000, 6000, 0);
    m_fgaussz->SetNpx(1e4);

    m_fpolya = new TF1("fpolya",this, &SimuElectrons::PolyaDistrib,0,1000000,0);
    m_fpolya->SetNpx(1e4);

	TableReader();
	BuildCOM(m_meanComPoisson,m_fanoFactor,true);

    m_HZtrace = new TH1F("HZtrace",";Track duration; entries",maxtrack,0,maxtrack); // on initialise également un histogram qui est utliser pour pouvoir réinitialiser un nombre d'événement tout en gardant en mémoir la durée mesurer par la simulation
}



double SimuElectrons::GetRDMComPoisson(){double draw = gen->Uniform(0.,1.) ; return cdfDraw(draw) ;}; //mettre en inline



///--------------------------///

// on rajoute un électron à un événement
void SimuElectrons::AddElectronToStack(Electrons elec){
    m_vElectrons.push_back(elec);
}
// on rajoute un événement (tous les événement seront stocké dans m_vtrace
void SimuElectrons::AddTraceTostack(vector<Electrons> elec){
    m_vtraces.push_back(elec);
}


// fait drifté/dérivé les électrons de tout les événements
void SimuElectrons::gaussdrift(){

    for(int i=0; i<m_vtraces.size(); i++){
        for(int j=0; j<m_vtraces[i].size();j++){
            double rdmx = m_fgaussxy->GetRandom();
            double rdmy = m_fgaussxy->GetRandom();
            double rdmz = m_fgaussz->GetRandom();
            m_vtraces[i][j].x= m_vtraces[i][j].x +rdmx ;
            m_vtraces[i][j].y= m_vtraces[i][j].y +rdmy ;
            m_vtraces[i][j].z= m_vtraces[i][j].z +rdmz ;
            m_vtraces[i][j].energy= 1 ;// 0.5 * MASSEELECTRON * pow(m_vdrift * 1000 ,2 )* JTOEV ;
        }
    }

}


//Fait l'avalanche (donne juste le nombre d'éléctron arraché pendant l'avalanche dans la variable energy de chaque électron)
//Le nombre d'électron final est trop important (150.000) pour pouvoir les rajouté dans un événement
void SimuElectrons::Avalanch(){

    for(int k =0; k<m_vtraces.size(); k++){

        for(int i=0; i<m_vtraces[k].size() ; i++){

            int rdm = m_fpolya->GetRandom();
            m_vtraces[k][i].energy = rdm;
        }
    }
}


//Retourne la distance Z entre le premier et le dernier electron d'un événement k
double SimuElectrons::tracelenghtZ(int k){
    if (m_vtraces[k].size()==0){return 0;}//can happen with poisson distrib
    double Mx = m_vtraces[k][0].z;
    double mn = m_vtraces[k][0].z;
    for(int i = 0; i<m_vtraces[k].size(); i++){
        if(m_vtraces[k][i].z>Mx){Mx=m_vtraces[k][i].z;}
        if(m_vtraces[k][i].z<mn){mn=m_vtraces[k][i].z;}
    }
    return Mx-mn ;

}

//Comparateur pour trié les electrons selon les Z croissant
bool compare(SimuElectrons::Electrons E1, SimuElectrons::Electrons E2){
        return (E1.z <E2.z);
    }
//trie les electron selont les Z croissant
void SimuElectrons::SortTrace(int k){

    if(m_vtraces[k].size()>1){
        sort(m_vtraces[k].begin(),m_vtraces[k].end(),compare);
    }
}

//Mesure la durée du signal d'un événement k
double SimuElectrons::TrueDuration(int k){

    if(m_vtraces[k].size()>0){
        SortTrace(k); //on trie les événement selon les Z croissant (nécessaire pour la fonction IonCurrent)

        //m_meanz += ((m_vtraces[k][1].z - m_vtraces[k][0].z)*1e-6)/(m_vdrift*1000); //décomenter si on veut la moyenne des écart entre les éléctrons

        TF1 *f1= new TF1("f1",this,&SimuElectrons::IonCurent,-2.5e-6,2.5e-6,20); //la fonction du signal

        for(int i=0;i <20; i++){f1->SetParameter(i,155);} //on initialise les paramétre avec une valeur impossible a prendre (ici 155) zero peut étre prise méme si trés improbable de tombé exactement dessu

        for(int i=0;i<m_vtraces[k].size()*2;i+=2){

            double zt = (m_vtraces[k][i/2].z*1e-6)/(m_vdrift*1000);
            int parr = i;
            f1->SetParameter(parr,zt); //on met la position Z de chaque electron dans les parrametre Pair
            f1->SetParameter(i+1,m_vtraces[k][i/2].energy); //on met l'energie de chaque electron dans les parrametre impaire
        }

        //les fonction findseuil sont trés lente et peuvent étre optimisé (elle sont définit juste en dessous)
        double zmin = FindSeuilMin(f1, m_seuil);
        double zmax = FindSeuilMax(f1, m_seuil);

        //décomenté si on souhaite afficher le signal d'un événement (ici le le premier)
       /*if(k==1){
            new TCanvas();
            f1->SetNpx(1e6);
            gPad->SetLogy();
            f1->Draw();
        }*/


        return zmax-zmin;
    }
    else{return 0;}
}

//trouve la valeur minimal pour la quelle le signal "TF1 *current" est inferieur au "double seuil"
double SimuElectrons::FindSeuilMin(TF1 *current, double seuil){
    double mini =0;
    for(double i = -2.5e-6; current->Eval(i)<seuil; i += 1e-9){ // l'incrémentaion peut étre augmenter pour allé plus vite mais perd en précision et risque de sauté une valeur seuil (le signal électronique est trés court)
        mini = i;
        if(i>1e-6){cout<< "!!! seuil trop haut ou espacement des electrons trop elevée (FindSeuilMin) !!!"<<endl;break; }//arréte la recherche si il ne trouve rien
    }
    return mini;
}

//trouve la valeur maximal pour la quelle le signal "TF1 *current" est inferieur au "double seuil"
double SimuElectrons::FindSeuilMax(TF1 *current, double seuil){
    double mini =0;
    for(double i = 2.5e-6; current->Eval(i)<seuil; i -= 1e-9){// l'incrémentaion peut étre augmenter pour allé plus vite mais perd en précision et risque de sauté une valeur seuil (le signal électronique est trés court)
        mini = i;
        if(i<-1e-6){cout<< "!!! seuil trop haut ou espacement des electrons trop elevée (FindSeuilMax) !!!"<<endl;break; }//arréte la recherche si il ne trouve rien
    }
    return mini;
}

//Donne l'energy (le nombre total d'éléctron) d'un évenement j
double SimuElectrons::TotalEnergy(int j){
    double energi;
    for(int i = 0; i<m_vtraces[j].size() ; i++){
        energi += m_vtraces[j][i].energy ;
    }
    return energi ;
}




//met la positionXZ des éléctron sur un Graph
TGraph* SimuElectrons::Showelectronxz(){
    TGraph *elecxz = new TGraph();
    for(int i=0;i<m_vElectrons.size();i++){
        elecxz->AddPoint(m_vElectrons[i].x,m_vElectrons[i].z);
    }
    return elecxz ;
}


//met la positionYZ des éléctron sur un Graph
TGraph* SimuElectrons::Showelectronyz(){
    TGraph *elecyz = new TGraph();
    for(int i=0;i<m_vElectrons.size();i++){
        elecyz->AddPoint(m_vElectrons[i].y,m_vElectrons[i].z);
    }
    return elecyz ;
}


//met la positionXY des éléctron sur un Graph
TGraph* SimuElectrons::Showelectronxy(){
    TGraph *elecxy = new TGraph();
    for(int i=0;i<m_vElectrons.size();i++){
        elecxy->AddPoint(m_vElectrons[i].x,m_vElectrons[i].y);
    }
    return elecxy ;
}

//met la positionXZ des éléctron dans un histograme (pour un but statistique)
TH2D* SimuElectrons::Showelectronhxz(){
    TH2D *elecxy = new TH2D("xz","xzbis",200,-4000,4000,200,-4000,4000);
    for(int i=0;i<m_vElectrons.size();i++){
        elecxy->Fill(m_vElectrons[i].x,m_vElectrons[i].z);
    }
    return elecxy ;
}

//met la positionYZ des éléctron dans un histograme (pour un but statistique)
TH2D* SimuElectrons::Showelectronhyz(){
    TH2D *elecxy = new TH2D("yz","yzbis",200,-4000,4000,200,-4000,4000);
    for(int i=0;i<m_vElectrons.size();i++){
        elecxy->Fill(m_vElectrons[i].y,m_vElectrons[i].z);
    }
    return elecxy ;
}

//met la positionXY des éléctron dans un histograme (pour un but statistique)
TH2D* SimuElectrons::Showelectronhxy(){
    TH2D *elecxy = new TH2D("xy","xybis",200,-4000,4000,200,-4000,4000);
    for(int i=0;i<m_vElectrons.size();i++){
        elecxy->Fill(m_vElectrons[i].x,m_vElectrons[i].y);
    }
    return elecxy ;
}

//montre les graph de la position des éléctron
void SimuElectrons::Graphelectron(){
    int boxsize = 6000  ;

    TCanvas *celecpos = new TCanvas("celecposition");
    celecpos->Divide(3,1);

    celecpos->cd(1);
    celecpos->cd(1)->SetLeftMargin(0.13);
    TGraph* xy = SimuElectrons::Showelectronxy();
    xy->GetXaxis()->SetLimits(m_vElectrons[0].x-boxsize,m_vElectrons[0].x+boxsize);
    xy->GetXaxis()->SetTitle("x");
    xy->SetMinimum(m_vElectrons[0].y-boxsize);
    xy->SetMaximum(m_vElectrons[0].y+boxsize);
    xy->GetYaxis()->SetTitle("y");
    xy->SetMarkerStyle(PTSTYLE);
    xy->SetLineWidth(0);
    xy->Draw();

    celecpos->cd(2);
    celecpos->cd(2)->SetLeftMargin(0.13);
    TGraph* xz = SimuElectrons::Showelectronxz();
    xz->GetXaxis()->SetLimits(m_vElectrons[0].x-boxsize,m_vElectrons[0].x+boxsize);
    xz->GetXaxis()->SetTitle("x");
    xz->SetMinimum(m_vElectrons[0].z-boxsize);
    xz->SetMaximum(m_vElectrons[0].z+boxsize);
    xz->GetYaxis()->SetTitle("z");
    xz->SetMarkerStyle(PTSTYLE);
    xz->SetLineWidth(0);
    xz->Draw();

    celecpos->cd(3);
    celecpos->cd(3)->SetLeftMargin(0.13);
    TGraph* yz = SimuElectrons::Showelectronyz();
    yz->GetXaxis()->SetLimits(m_vElectrons[0].y-boxsize,m_vElectrons[0].y+boxsize);
    yz->GetXaxis()->SetTitle("y");
    yz->SetMinimum(m_vElectrons[0].z-boxsize);
    yz->SetMaximum(m_vElectrons[0].z+boxsize);
    yz->GetYaxis()->SetTitle("z");
    yz->SetMarkerStyle(PTSTYLE);
    yz->SetLineWidth(0);
    yz->Draw();


}

//montre les histogram de la position des éléctrons
void SimuElectrons::Histelectron(){

    TCanvas *celecposh = new TCanvas("celecpositionh");
    celecposh->Divide(3,1);

    celecposh->cd(1);
    celecposh->cd(1)->SetLeftMargin(0.13);
    TH2D* xy = SimuElectrons::Showelectronhxy();
    xy->GetXaxis()->SetTitle("x");
    xy->GetYaxis()->SetTitle("y");
    xy->SetMarkerStyle(PTSTYLE);
    xy->Draw("COL");

    celecposh->cd(2);
    celecposh->cd(2)->SetLeftMargin(0.13);
    TH2D* xz = SimuElectrons::Showelectronhxz();
    xz->GetXaxis()->SetTitle("x");
    xz->GetYaxis()->SetTitle("z");
    xz->SetMarkerStyle(PTSTYLE);
    xz->Draw("COL");

    celecposh->cd(3);
    celecposh->cd(3)->SetLeftMargin(0.13);
    TH2D* yz = SimuElectrons::Showelectronhyz();
    yz->GetXaxis()->SetTitle("y");
    yz->GetYaxis()->SetTitle("z");
    yz->SetMarkerStyle(PTSTYLE);
    yz->Draw("COL");


}


//graph la distribution du nombre primaire de tout les évenement
void SimuElectrons::GraphPrimary(){
    TH1F *HPrimary = new TH1F("HPrimary",";Nombre electrons; entries",15,0,15);
    for(int i=0; i<m_vtraces.size() ; i++){
        HPrimary->Fill(m_vtraces[i].size());
    }
    TCanvas *cPrimary = new TCanvas("cPrimary");
    cPrimary->SetLeftMargin(0.13);
    HPrimary->Draw();
}

//graph la durée du signal de tout les événement ("int maxtrack est la durée en time slice la plus longue affiché sur le graph)
void SimuElectrons::GraphZtrace(int maxtrack){
    TH1F *Hztrace = new TH1F("Hztrace",";Track duration; entries",maxtrack,0,maxtrack);
    m_meanz=0;
    for(int i=0; i<m_vtraces.size() ; i++){
        double duration = TrueDuration(i);
        Hztrace->Fill((duration/(20.e-9))*1);
        m_HZtrace->Fill((duration/(20.e-9))*1);
    }
    //cout<<"meanz : "<<((m_meanz/20e-9)/2000)<<endl; // décomenté pour affiché la moyenne de l'écart entre les éléctrons 2000 est le nombre d'événement total (à changé)

    new TCanvas();
    Hztrace->SetLineColor(1);
    Hztrace->SetLineWidth(2);
    Hztrace->SetFillColor(43);
    Hztrace->SetFillStyle(3001);
    Hztrace->Draw();
}

//graph l'énergie de chaque evenement pour chaque nombre d'éléctron primaire ("int high" est l'énergie maximal affiché sur le graph)
void SimuElectrons::GraphEnergy(int high){
    int siz = 1500000;

    TH1F *henergy1 = new TH1F("henergi1",";energie/nbr electron; entries",50,0,siz);
    henergy1->SetMaximum(high);
    TH1F *henergy2 = new TH1F("henergi2",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy3 = new TH1F("henergi3",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy4 = new TH1F("henergi4",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy5 = new TH1F("henergi5",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy6 = new TH1F("henergi6",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy7 = new TH1F("henergi7",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy8 = new TH1F("henergi8",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy9 = new TH1F("henergi9",";energie/nbr electron; entries",50,0,siz);
    TH1F *henergy10= new TH1F("henergi10",";energie/nbr electron; entries",50,0,siz);
    henergy1->SetFillColor(40);
    henergy2->SetFillColor(41);
    henergy3->SetFillColor(42);
    henergy4->SetFillColor(43);
    henergy5->SetFillColor(44);
    henergy6->SetFillColor(45);
    henergy7->SetFillColor(46);
    henergy8->SetFillColor(47);
    henergy9->SetFillColor(48);
    henergy10->SetFillColor(2);

    for(int i=0; i<m_vtraces.size(); i++){

        if(m_vtraces[i].size()>0){

            cout << "" ;//pour une raison inconnue affiché quelque chose à cette endroit du code le fait fonctioné
            if     (m_vtraces[i][0].nbrprimaire==1){henergy1->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==2){henergy2->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==3){henergy3->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==4){henergy4->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==5){henergy5->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==6){henergy6->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==7){henergy7->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==8){henergy8->Fill(this->TotalEnergy(i));}
            else if(m_vtraces[i][0].nbrprimaire==9){henergy9->Fill(this->TotalEnergy(i));}
        }
    }
    TCanvas *cEnergy = new TCanvas("cEnergy");
    cEnergy->SetLeftMargin(0.13);
    henergy1->Draw("same");
    henergy2->Draw("same");
    henergy3->Draw("same");
    henergy4->Draw("same");
    henergy5->Draw("same");
    henergy6->Draw("same");
    henergy7->Draw("same");
    henergy8->Draw("same");
    henergy9->Draw("same");
    henergy10->Draw("same");

    //décomenté pour affiché des fit gaussien
    /*
    henergy1->Fit("gaus");
    henergy2->Fit("gaus");
    henergy3->Fit("gaus");
    henergy4->Fit("gaus");
    henergy5->Fit("gaus");
    henergy6->Fit("gaus");
    henergy7->Fit("gaus");
    henergy8->Fit("gaus");
    */

}

//affiche la durée du signal pour chaque nombre de charge primaire
void SimuElectrons::GraphAllZDuration(){

    TH1F *hzn1 = new TH1F("hzn1",";durée; entries",50,0,50);
    TH1F *hzn2 = new TH1F("hzn2",";durée; entries",50,0,50);
    TH1F *hzn3 = new TH1F("hzn3",";durée; entries",50,0,50);
    TH1F *hzn4 = new TH1F("hzn4",";durée; entries",50,0,50);
    TH1F *hzn5 = new TH1F("hzn5",";durée; entries",50,0,50);
    TH1F *hzn6 = new TH1F("hzn6",";durée; entries",50,0,50);
    TH1F *hzn7 = new TH1F("hzn7",";durée; entries",50,0,50);
    TH1F *hzn8 = new TH1F("hzn8",";durée; entries",50,0,50);
    TH1F *hzn9 = new TH1F("hzn9",";durée; entries",50,0,50);
    hzn1->SetFillColor(40);
    hzn2->SetFillColor(41);
    hzn3->SetFillColor(42);
    hzn4->SetFillColor(43);
    hzn5->SetFillColor(44);
    hzn6->SetFillColor(45);
    hzn7->SetFillColor(46);
    hzn8->SetFillColor(47);
    hzn9->SetFillColor(48);

    for(int i=0; i<m_vtraces.size(); i++){

        if(m_vtraces[i].size()>0){

            cout << "" ;//ca peut bugé si on enléve le cout
            if     (m_vtraces[i][0].nbrprimaire==1){hzn1->Fill(this->TrueDuration(i))/20.e-9;}
            else if(m_vtraces[i][0].nbrprimaire==2){hzn2->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==3){hzn3->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==4){hzn4->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==5){hzn5->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==6){hzn6->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==7){hzn7->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==8){hzn8->Fill(this->TrueDuration(i)/20.e-9);}
            else if(m_vtraces[i][0].nbrprimaire==9){hzn9->Fill(this->TrueDuration(i)/20.e-9);}
        }
    }
    TCanvas *cnZduration = new TCanvas("cnZduration");
    cnZduration->Divide(3,3);

    cnZduration->cd(1);
    cnZduration->cd(1)->SetLeftMargin(0.13);
    hzn1->Draw();

    cnZduration->cd(2);
    cnZduration->cd(2)->SetLeftMargin(0.13);
    hzn2->Draw();

    cnZduration->cd(3);
    cnZduration->cd(3)->SetLeftMargin(0.13);
    hzn3->Draw();

    cnZduration->cd(4);
    cnZduration->cd(4)->SetLeftMargin(0.13);
    hzn4->Draw();

    cnZduration->cd(5);
    cnZduration->cd(5)->SetLeftMargin(0.13);
    hzn5->Draw();

    cnZduration->cd(6);
    cnZduration->cd(6)->SetLeftMargin(0.13);
    hzn6->Draw();

    cnZduration->cd(7);
    cnZduration->cd(7)->SetLeftMargin(0.13);
    hzn7->Draw();

    cnZduration->cd(8);
    cnZduration->cd(8)->SetLeftMargin(0.13);
    hzn8->Draw();

    cnZduration->cd(9);
    cnZduration->cd(9)->SetLeftMargin(0.13);
    hzn9->Draw();



}

