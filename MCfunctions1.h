#ifndef MCFUNCTIONS_H_INCLUDED
#define MCFUNCTIONS_H_INCLUDED

//C++
#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include <vector>

//ROOT
#include "TROOT.h"
#include "TFile.h"
#include "TMath.h"
#include "TTree.h"
#include "TF1.h"
#include "TF3.h"

#include "TCanvas.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TApplication.h"
#include "TRandom3.h"




/*!
    \brief
        This class handles the transport of the electrons
*/

class SimuElectrons{

    public:

        SimuElectrons();//Constructor
        ~SimuElectrons();//Destructor

        ///Structure d'un electron
        //le paramétre t n'est pas utilisé
        struct Electrons{
            int nbrprimaire; // le nombre de charge primaire dans l'événement de cette électro
            double x;//in cm
            double y;//in cm
            double z;//in cm
            double t;//in seconds
            double energy;//en nbr nombre d'éléctrons
        };

        ///Set methods des differentes distributtions
        inline void SetMeanPoisson(double x){m_meanPoisson = x;};
        inline void SetDiffxy(double x){m_diffxy = x;};
        inline void SetDiffz(double x){m_diffz = x;};
        inline void SetVectorElectrons(std::vector<Electrons> x){m_vElectrons = x;};
        inline void SetMeanPolya(double x){m_meanPolya =x;};
        inline void SetFactorPolya(double x){m_factorPolya =x;};
        inline void SetVdrift(double x){m_vdrift =x;};
        inline void SetDriftLength(double x){m_driftlength =x;};
        inline void SetMeanComPoisson(double x){m_meanComPoisson = x;};
        inline void SetFanoFactor(double x){m_fanoFactor = x;};

        ///set methods des différent parametre du détecteur
        inline void SetSpeedElectron(double x){m_speedelectron =x;};
        inline void SetSpeedIon(double x){ m_speedion=x;};
        inline void SetIonlength(double x){ m_ionlength=x;};
        inline void SetTownsend(double x){m_townsend=x;};
        inline void SetSeuil(double x){m_seuil=x;};

        ///Get methods
        inline double GetMeanPoisson(){return m_meanPoisson;};
        inline double GetDiffxy(){return m_diffxy;};
        inline double GetDiffz(){return m_diffz;};
        inline std::vector<Electrons> GetVectorElectrons(){return m_vElectrons;};
        inline int GetVectorElectronsSize(){return m_vElectrons.size();};
        inline double GetMeanPolya(){return m_meanPolya;};
        inline double GetFactorPolya(){return m_factorPolya;};
        inline double GetVdrift(){return m_vdrift;};
        inline double GetDriftLength(){return m_driftlength;};
        inline std::vector<std::vector<Electrons>>GetVTrace(){return m_vtraces;};

        ///Get RDM from distrib method
        inline double GetRDMGaussXY(){return m_fgaussxy->GetRandom();};
        inline double GetRDMGaussZ(){return m_fgaussz->GetRandom();};
        inline double GetRDMPoisson(){return m_fpoisson->GetRandom();};
        inline double GetRDMPolya(){return m_fpolya->GetRandom();};
        double GetRDMComPoisson();

        ///clear methods
        inline void ClearVectorElectrons(){m_vElectrons.clear() ; }
        inline void ClearTraces(){m_vtraces.clear();}

        ///affiche le graph stocké dans la class (initialisé par CreateDistribs remplie par GraphZtrace)
        inline void ShowGraphduration(){m_HZtrace->Draw();};


        ///distrib Methode (utilisé dans un TF1)
        double PoissonDistribution(double *x, double *par);
        double GaussianDistributionxy(double *x, double *par);
        double GaussianDistributionz(double *x, double *par);
        double PolyaDistrib(double *x, double *par);

        ///stocke les distribution dans la simu
        void CreateDistribs(int maxtrack); //utilise les methode de distribition (dans des TF1)


        ///add electron/ évenment
        void AddElectronToStack(Electrons elec);
        void AddTraceTostack(std::vector<Electrons> elec);

        ///Génére les événement dans le vecteur de vecteur m_vtraces;
        void CasioGenerate(char* file);
        void ComPoissonGenerate(int nevenement);
        void LinearGenerate(int nelec, int nevenement);

        ///les deux étape du detecteur
        void gaussdrift();
        void Avalanch();

        ///Affiche la position des electron
        void Graphelectron();
         TGraph* Showelectronxz();
         TGraph* Showelectronyz();
         TGraph* Showelectronxy();
        ///Affichel la position des electrons dans un histo dans un but statistique
        void Histelectron();
         TH2D* Showelectronhxz();
         TH2D* Showelectronhyz();
         TH2D* Showelectronhxy();

        ///donne la distance entre le premier et le dernier électron de l'événement k
        double tracelenghtZ(int k);

        ///Graph la distriution de charge primaire de toute les événements
        void GraphPrimary();

        ///Graph la "mesure" de la durée du signal de chaque évenment
        void GraphZtrace(int maxtrack);
         double TrueDuration(int k); // Mesure la durée du signal d'un événement k
          double IonCurent(double *x, double *par); //La fonction IonCurent est mis dans un TF1
            void SortTrace(int k); //la fct Ioncurrent a besoin que les chaque électrons d'un événement soit triér
          double FindSeuilMin(TF1 *current, double seuil);//cherche la premiére valeur pour laquelle le signal dépasse le seuil
          double FindSeuilMax(TF1 *current, double seuil);//cherche la derniere valeur pour laquelle le signal dépasse le seuil

        ///Graph l'énergie de chaque événénement  (un histo pour chaque nombre de charge primaire)
        void GraphEnergy(int high);
         double TotalEnergy(int j);//mesure l'energie d'un événement (le nombre d'éléctron aprés avalance = energi)

        ///Graph la "mesure" de la durée du signal de chaque évenement (un histo pour chaque nombre de charge primaire)
        void GraphAllZDuration();// utilise la fonction TrueDuration

    private:

        ///Simulation parameters
        double m_meanPoisson;
        double m_diffxy;
        double m_diffz;
        double m_meanPolya;
        double m_factorPolya;
        double m_meanComPoisson;
        double m_fanoFactor;

        ///Instrument parameter
        double m_speedelectron;
        double m_speedion;
        double m_ionlength;
        double m_townsend;
        double m_vdrift;
        double m_driftlength;
        double m_seuil;

        ///Vector of primary electrons
        std::vector<Electrons> m_vElectrons; // for one trace
        std::vector<std::vector<Electrons>> m_vtraces; // all the trace

        ///Distribution

        TF1 *m_fgaussxy;
        TF1 *m_fgaussz;
        TF1 *m_fpolya;
        TF1 *m_fcompoisson;
        TF1 *m_fpoisson;

        ///Histo

        TH1F *m_HZtrace ;

        double m_meanz ;


};


#endif //MCFUNCTIONS_H_INCLUDED
