#include "SimuPrimaryElectrons1.h"

//----Definition constant physique----\\




using namespace std;

/*!
    \brief Build a Poisson distribution from a MC

*/

int main(int argc, char** argv){

    //getting argument
    char mode = argv[1][0]; // le premier définit la méthode de géneration des charge primaire
    char* file = argv[2]; // le fichier .dat pour la méthode casino

    //Initialize the SimuElectron class
    SimuElectrons *simu = new SimuElectrons();
    gRandom->SetSeed(time(NULL));//Set the seed of the random number generator

    //Instrument parameters
    simu->SetVdrift(12.3) ; //en µm/ns
    simu->SetDriftLength(5.0) ; //en cm
    simu->SetSpeedElectron(500000); //en m/s
    simu->SetSpeedIon(1500);  //en m/s
    simu->SetIonlength(512e-6);  //en m
    simu->SetTownsend(23200);
    simu->SetSeuil(0.2e-6); //en ampére

    //distribution parameters
    simu->SetMeanPoisson(4.0) ; // no unit
    simu->SetDiffxy(sqrt(2)*235*sqrt(simu->GetDriftLength()));//en µm/cm^0.5
    simu->SetDiffz(sqrt(2)*290*sqrt(simu->GetDriftLength()));//en µm/cm^0.5
    simu->SetFactorPolya(10); //no unit
    simu->SetMeanPolya(150000); // nombre d'électron
    simu->SetMeanComPoisson(5); //no unit
    simu->SetFanoFactor(0.3); //no unit



    TApplication *theApp = new TApplication("theApp", &argc, argv); ///the app for canvas

    simu->CreateDistribs(40); //créé toute les distribution aléatoire (la distribution com-poisson est un peut lente et peut être supprimés si non nécessaire)

///=============================///
/// les différentes méthodes
///=============================///


    ///////////////////
    ///casino method///
    ///////////////////



    if(mode=='1'){
        simu->CasioGenerate(file); // génere tout les évenement d'un fichier casino .dat
        simu->gaussdrift() ; //application du drift
        simu->GraphPrimary(); // affiche la distribution du nombre primaire d'électron
        simu->Avalanch(); // multiplie l'énergie des électrons généré par la polya
        simu->GraphZtrace(40); // affichel la durée de chaque evenement
        simu->GraphEnergy(100); // affiche l'énergie pour chaque nombre d'électron primaire
    }


    ////////////////////////
    ///Com Poisson method///
    ////////////////////////


    else if(mode=='2'){
        simu->ComPoissonGenerate(10000); // génere X évenement avec la compoisson
        simu->gaussdrift(); //application du drift
        simu->GraphPrimary(); // affiche la distribution du nombre primaire d'électron
        simu->Avalanch(); // multiplie l'énergie des électrons généré par la polya
        simu->GraphZtrace(40); // affichel la durée de chaque evenement
        simu->GraphEnergy(1500); // affiche l'énergie pour chaque nombre d'électron primaire
    }

    ///////////////////
    ///Linear method///
    ///////////////////

    else if(mode=='3'){

    for(int i=1;i<2;i++){ // i est le nombre de charge primaire


        simu->ClearTraces(); //purge tout les événement
        simu->LinearGenerate(i,100); // gener X événement avec i charge primaire
        simu->gaussdrift(); //application du drift
        simu->Avalanch(); // multiplie l'énergie des électrons généré par la polya
        simu->GraphZtrace(40); // affiche la durée de chaque evenement pour ce loop
        simu->Avalanch(); // multiplie l'énergie des électrons généré par la polya
        }
    new TCanvas();
    simu->ShowGraphduration(); // // affiche la durée de chaque evenement pour chaque loop

    }

    //si mauvais argument
    else {cerr << "wrong number"<<endl;}


    theApp->Run();

    delete simu;
    return 0;
}
