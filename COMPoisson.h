#ifndef COMPOISSON_H_INCLUDED
#define COMPOISSON_H_INCLUDED

using namespace std;
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "TChain.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TH1.h"
#include "TF1.h"
#include "TF2.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TRint.h"
#include "TROOT.h"
#include "TGaxis.h"
#include "TApplication.h"
#include "TFile.h"
//#include "Math/Interpolator.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TFormula.h"
#include "TLine.h"
#include "TColor.h"
#include "TGraph2D.h"
#include "Math/WrappedTF1.h"
#include "Math/WrappedMultiTF1.h"
//#include "Math/GSLMCIntegrator.h"
//#include "Math/GSLIntegrator.h"
#include "Math/AdaptiveIntegratorMultiDim.h"
#include <iomanip>

// to clear an std vector
template <typename VecType>
void freeFromMemory(VecType& myVec) {
     myVec.clear();
     VecType vert;
     myVec.swap(vert);
}

// convert anything into string
template<typename T>string to_str(const T & aconvertir)
{
     	ostringstream oss;
     	oss << aconvertir;
     	string sfromstream=oss.str();
     	return sfromstream;
}

//-----------------------------------------------------------------------------------------
//-----------------------------------Function Declarations---------------------------------
//-----------------------------------------------------------------------------------------

//-----     Primary Functions     -----

void BuildCOM(double mu, double F, bool verbose = false);
double PCOM(double N, double mu, double F, bool verbose);
double PCOM_Fast(double N, double mu, double F);
void BuildCOM_Fast(double mu, double F);
void BuildHisto(int n);
int cdfDraw(double draw);
void TableReader();

//-----     Secondary Functions     -----

Double_t Z(double l, double v);
Double_t COM(Double_t *palpha, Double_t *par);
void Moments(double l, double v);
void cdfBuild();
void cdfBuild_FAST();
double Stirling(int n);
bool isBernoulli(double mu, double F);
bool isSoft(double mu, double F, double tol);
double linspace(double a, double b, double n, double i);
double logspace(double n, double a, double b, double i);
void BiTerp(double mu_draw, double F_draw);
void LV_asymp(double mu, double F);
void cdfBernoulli();
void cdfGauss();
void Zombie_Fix(double mu, double F, double tol, bool verbose);
double Bernoulli(double mu);
double pdfBernoulli(double N, double mu);
Double_t Z_asymp(double l, double v);
Double_t logZ(double l, double v);
void Moments_Asymp(double l, double v);
void BinLogX(TH1*h);
void Gerror(double l, double v, double mu_want, double F_want);

//-----------------------------------------------------------------------------------------
//-------------------------------------Global Variables------------------------------------
//-----------------------------------------------------------------------------------------

// T-Objects
TRandom *gen = new TRandom3();					// random number generator
TF1 *f_COM = new TF1("f_COM",COM,0,200,2);		// TF1 for COM distribution

// Other global variables
Double_t G_l,G_v;					// lambda and nu
Double_t G_mu,G_var,G_F;			// mean, variance, and Fano factor
std::vector<double> cdf;			// std::vec for cdf of COM-Poisson distribution
int cdf_a,cdf_b,cdf_n;				// global definition of cdf
Double_t G_mu_e, G_var_e, G_F_e;	// mean, variance, F errors
Double_t v_asymp,l_asymp;			// asymptotic approximations for lambda and nu

//-----------------------------------------------------------------------------------------
//-----------------------------------Definition of Tables----------------------------------
//-----------------------------------------------------------------------------------------

// name and location of table ---- NEED TO CHANGE "TableDir": see "README"
string TableName = "Table4_Alpha";					// string name of table
string TableDir = "";	// Directory where Table is stored

// Below, the parameters of the table to be used are globally defined
const int Mu_N = 10000;			// number of values of mean
const int F_N = 1000;			// number of values of Fano Factor
double Mu_a = 0.001;			// lowest limit of mean
double Mu_b = 20.;				// upper limit of mean
double F_a = 0.1;				// lower limit of Fano Factor
double F_b = 1.;				// upper limit of Fano Factor

// Table Storgae
double L_Table[Mu_N][F_N];
double V_Table[Mu_N][F_N];
std::vector<double> L_store;
std::vector<double> V_store;
double F_List[F_N];
TGraph *l_single[10];
TGraph *v_single[10];

//--- Set value of mu at which code switches to a Gaussian (default is 100, max is 110)
double G_Transition = 100.;

//-----------------------------------------------------------------------------------------
//--------------------------------------Main Function--------------------------------------
//-----------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
//------------------------------------Primary Functions------------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------

//-----------------BuildCOM---------------------
// Code to find COM parameters (default with BiTerp)
// by using interpolation and asymptotic expressions
// Then calculates moments, sets up COM TF1, builds CDF
//
// INPUTS:	mu, F
// 			bool verbose = option for cout of info
//
// OUTPUT:	G_l and G_v stored globally
// 			G_mu and G_var stored globally
// 			CDF store globally
// 			COM TF1 set up
//----------------------------------------------
void BuildCOM(double mu, double F, bool verbose){

	// If the point is forbidden, this exits the function
	if(isSoft(mu,F,0.001) == false && isBernoulli(mu,F) == true){
		cout<<"  !!!!!   This pair of points is inside the Bernoulli modes   !!!!!"<<endl;
		exit(1);
	}

	// Declare variables
	double err_mu,err_F,mu_err1,F_err1,mu_err2,F_err2;

	// First, check for Bernoulli mode
	bool bernoulli = false;
	if(isSoft(mu,F,0.001) == true || isBernoulli(mu,F) == true){bernoulli = true;}

	// Check for Poisson condition
	bool poisson = false;
	if(F == 1.){
		poisson = true;
		bernoulli = false;
	}

	// Then check gauss
	bool gauss = false;
	if(mu > G_Transition && poisson == false){gauss = true;}

	// Next, perform asymptotic approximation (and calc error)
	LV_asymp(mu,F);
	Moments(l_asymp,v_asymp);
	mu_err2 = TMath::Abs(mu-G_mu)/mu;
	F_err2 = TMath::Abs(F-(G_var/G_mu))/F;

	// Use asymptotic approximation
	bool asymp = false;
	if(mu > 20. && gauss == false && poisson == false){asymp = true;}

	// Choose what to use!
	if(bernoulli == false && asymp == false && gauss == false && poisson == false){

		// Using table if needed (otherwise use singles!)
		if( (((int)(F*1000000))%100000) == 0 ){
			G_l = TMath::Power(10.,l_single[(int)(F*10)-1]->Eval(mu));
			G_v = v_single[(int)(F*10)-1]->Eval(mu);
		}else{BiTerp(mu,F);}

		// calculate error
		Moments(G_l,G_v);
		mu_err1 = TMath::Abs(mu-G_mu)/mu;
		F_err1 = TMath::Abs(F-(G_var/G_mu))/F;

		// use this or asymp?
		if(TMath::Sqrt(TMath::Power(mu_err1,2.) + 0.5*TMath::Power(F_err1,2.)) > TMath::Sqrt(TMath::Power(mu_err2,2.) + 0.5*TMath::Power(F_err2,2.))){asymp = true;}
	}

	// re-assign if using asymp
	if(asymp == true){
		G_l = l_asymp;
		G_v = v_asymp;
	}

	// calculate moments
	if(gauss == false){Moments(G_l,G_v);}

	// Use Bernoulli
	if(bernoulli == true){
		G_mu = mu;
		G_var = Bernoulli(mu);
	}

	// Use gauss
	if(gauss == true){
		G_mu = mu;
		G_var = (F*mu);
	}

	// use poisson
	if(poisson == true){
		G_l = mu;
		G_mu = mu;
		G_v = 1.;
		G_var = mu;
	}

	// calculate error
	err_mu = TMath::Abs((mu - G_mu)/mu);
	err_F = TMath::Abs((F - (G_var/G_mu))/F);

	// final option if error is high (tables/asymp only)
	bool zombie = false;
	if(err_mu > 0.001 || err_F > 0.001){
		if(bernoulli == false && gauss == false && poisson == false){
			Zombie_Fix(mu,F,0.001,verbose);
			zombie = true;
			err_mu = G_mu_e;
			err_F = G_F_e;
		}
	}

	// print out info if needed
	string method;
	if(bernoulli == true){
		method="Bernoulli";
	}else if(asymp == true){
		method="Asymptotic Expression";
	}else if(gauss == true){
		method = "Gaussian";
	}else if(poisson == true){
		method = "Poisson";
	}else if(zombie == true){
		method = "Zombie";
	}else{method="Tables";}

	if(verbose == true){
		cout<<" "<<endl;
		cout<<"   ------------------------------------------------------------     "<<endl;
		cout<<"     COM Parameters: lambda = "<<G_l<<", nu = "<<G_v<<endl;
		cout<<"     Results: mean = "<<G_mu<<", F = "<<(G_var/G_mu)<<endl;
		cout<<"     Error: mean_error = "<<100.*err_mu<<"%, F_error = "<<100.*err_F<<"%"<<endl;
		cout<<"     Method used: "<<method<<endl;
		cout<<"   ------------------------------------------------------------     "<<endl;
		cout<<" "<<endl;
	}

	// Build COM and CDF
	if(bernoulli == true){
		cdfBernoulli();
	}else if(gauss == true){
		cdfGauss();
	}else{
		f_COM->SetParameters(G_l,G_v);
		cdfBuild_FAST();
	}

	// Done! :)
}

//----------------------P_COM---------------------
// Code to give P(N|mu,F) with appropriate method
//
// INPUTS:	N, mu, F
// 			bool verbose = option for cout of info
//
// OUTPUT:	P(N|mu,F)
//----------------------------------------------
double PCOM(double N, double mu, double F, bool verbose){

	// If the point is forbidden, this exits the function
	if(isSoft(mu,F,0.001) == false && isBernoulli(mu,F) == true){
		cout<<"  !!!!!   This pair of points is inside the Bernoulli modes   !!!!!"<<endl;
		exit(1);
	}

	// Declare variables
	double err_mu,err_F,mu_err1,F_err1,mu_err2,F_err2;

	// Check for Poisson condition
	bool poisson = false;
	if(F == 1.){poisson = true;}

	// Check for Bernoulli mode
	bool bernoulli = false;
	if(isSoft(mu,F,0.001) == true || isBernoulli(mu,F) == true){bernoulli = true;}

	// Check Gaussian condition
	bool gauss = false;
	if(mu > G_Transition && poisson == false){gauss = true;}

	// Next, perform asymptotic approx. (and calculate error)
	LV_asymp(mu,F);
	Moments(l_asymp,v_asymp);
	mu_err2 = TMath::Abs(mu-G_mu)/mu;
	F_err2 = TMath::Abs(F-(G_var/G_mu))/F;

	// Use asymptotic approx, if appropriate
	bool asymp = false;
	if(mu > 20. && gauss == false && poisson == false){asymp = true;}

	// If not
	if(bernoulli == false && asymp == false && gauss == false && poisson == false){

		// Using table if needed (otherwise use singles!)
		if( (((int)(F*10))%1) == 0 ){
			G_l = TMath::Power(10.,l_single[(int)(F*10)-1]->Eval(mu));
			G_v = v_single[(int)(F*10)-1]->Eval(mu);
		}else{BiTerp(mu,F);}

		// calculate error
		Moments(G_l,G_v);
		mu_err1 = TMath::Abs(mu-G_mu)/mu;
		F_err1 = TMath::Abs(F-(G_var/G_mu))/F;

		// use this or asymp?
		if(TMath::Sqrt(TMath::Power(mu_err1,2.) + 0.5*TMath::Power(F_err1,2.)) > TMath::Sqrt(TMath::Power(mu_err2,2.) + 0.5*TMath::Power(F_err2,2.))){asymp = true;}
	}

	// re-assign if using asymp
	if(asymp == true){
		G_l = l_asymp;
		G_v = v_asymp;
	}

	// calculate moments
	if(gauss == false){Moments(G_l,G_v);}

	// Use Bernoulli
	if(bernoulli == true){
		G_mu = mu;
		G_var = Bernoulli(mu);
	}

	// Use gauss
	if(gauss == true){
		G_mu = mu;
		G_var = (F*mu);
	}

	// use poisson
	if(poisson == true){
		G_l = mu;
		G_mu = mu;
		G_v = 1.;
		G_var = mu;
	}

	// calculate error
	err_mu = TMath::Abs((mu - G_mu)/mu);
	err_F = TMath::Abs((F - (G_var/G_mu))/F);

	// final option if error is high (tables/asymp only)
	bool zombie = false;
	if(err_mu > 0.001 || err_F > 0.001){
		if(bernoulli == false && gauss == false && poisson == false){
			Zombie_Fix(mu,F,0.001,verbose);
			zombie = true;
			err_mu = G_mu_e;
			err_F = G_F_e;
		}
	}

	// print out info if needed
	string method;
	if(bernoulli == true){
		method="Bernoulli";
	}else if(asymp == true){
		method="Asymptotic Expression";
	}else if(gauss == true){
		method = "Gaussian";
	}else if(poisson == true){
		method = "Poisson";
	}else if(zombie == true){
		method = "Zombie";
	}else{method="Tables";}

	if(verbose == true){
		cout<<" "<<endl;
		cout<<"   ------------------------------------------------------------     "<<endl;
		cout<<"     COM Parameters: lambda = "<<G_l<<", nu = "<<G_v<<endl;
		cout<<"     Results: mean = "<<G_mu<<", F = "<<(G_var/G_mu)<<endl;
		cout<<"     Error: mean_error = "<<100.*err_mu<<"%, F_error = "<<100.*err_F<<"%"<<endl;
		cout<<"     Method used: "<<method<<endl;
		cout<<"   ------------------------------------------------------------     "<<endl;
		cout<<" "<<endl;
	}

	// Results!
	double P = 0.;
	if(bernoulli == true){
		P = pdfBernoulli(N,G_mu);
	}else if(gauss == true){
		P = TMath::Gaus(N,G_mu,G_var,kTRUE);
	}else{
		f_COM->SetParameters(G_l,G_v);
		P = f_COM->Eval(N);
	}

	// Done! :)
	return P;
}

//----------------------PCOM_Fast---------------
// Code to give P(N|mu,F)
// Same purpose as PCOM, but with less checks and
// comparisons, so it is faster but potentially
// slightly less accurate
//
// INPUTS:	N, mu, F
//
// OUTPUT:	P(N|mu,F)
//----------------------------------------------
double PCOM_Fast(double N, double mu, double F){

	// If the point is forbidden, this exits the function
	if(isSoft(mu,F,0.001) == false && isBernoulli(mu,F) == true){
		cout<<"  !!!!!   This pair of points is inside the Bernoulli modes   !!!!!"<<endl;
		exit(1);
	}

	// Declare variables
	double P = 0.;

	// Check for Poisson condition
	bool poisson = false;
	if(F == 1.){
		poisson = true;
		P = TMath::Poisson(N,mu);
		return P;
	}

	// Check for Bernoulli mode
	bool bernoulli = false;
	if(poisson == false){
		if(isSoft(mu,F,0.001) == true || isBernoulli(mu,F) == true){
			bernoulli = true;
			G_mu = mu;
			G_var = Bernoulli(mu);
			P = pdfBernoulli(N,G_mu);
			return P;
		}
	}

	// Gaussian Regime
	bool gauss = false;
	if(mu > G_Transition && poisson == false){
		gauss = true;
		P = TMath::Gaus(N,mu,TMath::Sqrt(F*mu),kTRUE);
		return P;
	}

	// Using COM-Poisson
	if(bernoulli == false && gauss == false && poisson == false){

		// Use guess?
		if(mu > 20. && gauss == false && poisson == false){
			LV_asymp(mu,F);
			G_l = l_asymp;
			G_v = v_asymp;

		// No? Then use tables
		}else{
			// Using Tables or TGraphs?
			if( (((int)(F*1000000))%100000) == 0 ){
				G_l = TMath::Power(10.,l_single[(int)(F*10)-1]->Eval(mu));
				G_v = v_single[(int)(F*10)-1]->Eval(mu);
			}else{BiTerp(mu,F);}
		}

		// Use COM-Poisson
		f_COM->SetParameters(G_l,G_v);
		P = f_COM->Eval(N);
	}

	// Done!
	return P;
}

//-----------------BuildCOM_Fast-----------------
// Faster, possibly less accurate version of BuildCOM:
// Same functionality as BuildCOM, but with less
// extra stuff, including some comparisons between
// appropriate methods to pick the most accurate
//
// INPUTS:	mu, F
//
// OUTPUT:	G_l and G_v stored globally
// 			G_mu and G_var stored globally
// 			CDF store globally
// 			COM TF1 set up
//----------------------------------------------
void BuildCOM_Fast(double mu, double F){

	// If the point is forbidden, this exits the function
	if(isSoft(mu,F,0.001) == false && isBernoulli(mu,F) == true){
		cout<<"  !!!!!   This pair of points is inside the Bernoulli modes   !!!!!"<<endl;
		exit(1);
	}

	// First, check for Bernoulli mode
	if(isSoft(mu,F,0.001) == true || isBernoulli(mu,F) == true){
		G_mu = mu;
		G_var = Bernoulli(mu);

		cdfBernoulli();
		return;

	// Check for Poisson condition
	}else if(F == 1.){
		G_l = mu;
		G_mu = mu;
		G_v = 1.;
		G_var = mu;
		Moments(G_l,G_v);
		f_COM->SetParameters(G_l,G_v);
		cdfBuild_FAST();
		return;

	// then check gauss
	}else if(mu > G_Transition && F != 1.){
		G_mu = mu;
		G_var = (F*mu);
		cdfGauss();
		return;

	// Use guess?
	}else if(mu > 20. && mu <= G_Transition){
		LV_asymp(mu,F);
		G_l = l_asymp;
		G_v = v_asymp;
		Moments(l_asymp,v_asymp);
		f_COM->SetParameters(G_l,G_v);
		cdfBuild_FAST();
		return;

	// Tables!
	}else{

		// Using table if needed (otherwise use singles!)
		if( (((int)(F*1000000))%100000) == 0 ){
			G_l = TMath::Power(10.,l_single[(int)(F*10)-1]->Eval(mu));
			G_v = v_single[(int)(F*10)-1]->Eval(mu);
		}else{BiTerp(mu,F);}

		Moments(G_l,G_v);
		f_COM->SetParameters(G_l,G_v);
		cdfBuild_FAST();
	}

	return;
	// Done! :)
}

//-----------------TableReader-------------------
// Loads COM Table as defined globally
//
// INPUTS: 	Global inputs
//
// OUTPUT:	stores tables in global arrays L_Table and V_Table
//----------------------------------------------
void TableReader(){

	// loop through files
	string sp = " ";
	string sl = "/";
	string end = ".txt";
	string Load;
	string fName;

	// clear temp storage vectors
	freeFromMemory(L_store);
	freeFromMemory(V_store);

	// progress
	string s = "=";
	cout<<" "<<endl;
	cout<<"               Building Tables. Progress:     "<<endl;
	cout<<" "<<endl;
	cout<<"  0%        20%      40%       60%       80%       100%"<<endl;
	cout<<"  |---------|---------|---------|---------|---------|"<<endl;
	cout<<"  ="<<std::flush;

	// loop over files, store in temp std vector
	for(int i = 0; i < F_N; i++){

		// get filename
		fName = to_str(linspace(F_a,F_b,F_N,i));
		Load = TableDir + TableName + sl + TableName + sp + fName + end;

		// opening file
		ifstream inFile;
		inFile.open(Load.c_str());

		// warning and escape if file can't be found
		if(!inFile){
			cout<<"Can't find the look-up table files! >:("<<endl;
			exit(1);
		}

		// reading result
		double hold[2];
		if (inFile.is_open()){
			while (!inFile.eof()){
				inFile >> hold[0] >> hold[1];

				L_store.push_back(hold[0]);
				V_store.push_back(hold[1]);
			}
		}

		// close file
		inFile.close();

		// move to table
		for(int j = 0; j < Mu_N; j++){
			L_Table[j][i] = L_store[j];
			V_Table[j][i] = V_store[j];
		}

		// clear vectors again
		freeFromMemory(L_store);
		freeFromMemory(V_store);

		// progress
		if( (i%(F_N/50)) == 0){
			cout<<s<<std::flush;
		}
	}

	// Build F_List, stored globally, for fast reading later
	for(int i = 0; i < F_N; i++){F_List[i] = linspace(F_a,F_b,F_N,i);}

	// build singles TGraphs
	for(int i = 0; i < 10; i++){

		// start new TGraph's
		l_single[(int)i] = new TGraph(Mu_N);
		v_single[(int)i] = new TGraph(Mu_N);

		for(int j = 0; j < Mu_N; j++){

			// set individual points
			l_single[(int)i]->SetPoint((int)j,logspace(Mu_a,Mu_b,Mu_N,j),L_Table[(int)j][(int)(i*111)]);
			v_single[(int)i]->SetPoint((int)j,logspace(Mu_a,Mu_b,Mu_N,j),V_Table[(int)j][(int)(i*111)]);
		}
	}

	// Done! :)
	cout<<" "<<endl;
	cout<<" "<<endl;
	cout<<"                  Done building tables!"<<endl;
	cout<<" "<<endl;
}

//------------------BuildHisto------------------
// Simple code to make histogram of COM
//
// INPUTS: 	G_mu,G_var taken globally
//			int n = number of points drawn
//
// OUTPUT:	histogram
//----------------------------------------------
void BuildHisto(int n){

	// make title
	string t1 = "#mu = ";
	string t2 = ", F = ";
	string t3 = ";X;P(X)";
	string title = t1 + to_str(G_mu) + t2 + to_str(G_var/G_mu) + t3;
	cout<<title<<endl;

	// set up plot
	TCanvas *c1 = new TCanvas("c1","c1");
    c1->cd();
	gStyle->SetOptStat(0);
	double a = (int)TMath::Max(0.,G_mu - 3.*G_var)-0.5;
	double b = (int)TMath::Max(10.,(G_mu + 3.*G_var))-0.5;
    TH1D *h1 = new TH1D("h1","#mu = 1, F = 0.5;x;",5*(b-a),a,b);

	// draw and fill histogram
	double draw;
	for(int i = 0; i < n; i++){
		// cdfBuild();
		draw = gen->Uniform(0.,1.);
		h1->Fill(cdfDraw(draw));
	}

	// plot
	h1->GetXaxis()->CenterTitle();
    h1->GetYaxis()->CenterTitle();
	h1->Draw();
}

//----------------cdfDraw-----------------------
// takes random real number (0,1) and gives integer
// drawn from COM cdf based on this
//
// Patch March 25 2020 by DD
//
// INPUTS: 	draw = random number drawn from [0,1)
//
// OUTPUT:	redraw = integer drawn from COM cdf
//----------------------------------------------
int cdfDraw(double draw){

	// initializing
	double val = 0.;

	// renormalize if needed
	bool renorm = false;
	if(draw > cdf[cdf_n-1]){
		renorm = true;
		do{ draw = gen->Uniform(0.,1.);
			if(draw > cdf[cdf_n-1]){
			renorm = true;
			}else{renorm = false;}
		}while(renorm == true);
	}

	// scanning cdf
	for(int j = 0; j < cdf_n; j++){
		if(draw > cdf[j]){val = cdf_a+j+1;}
	}

	// result
	int redraw = (int)val;

	// done :)
	return redraw;
}

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------Secondary Functions-----------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------

//-----------------Biterp-----------------------
// Regular grid bilinear interpolation, from wikipedia
//
// INPUTS:	mu_draw,F_draw
//
// OUTPUT:	G_l and G_v stored globally
//----------------------------------------------
void BiTerp(double mu_draw, double F_draw){

	// initialize
	double x = mu_draw;
	double y = F_draw;
	double x1,x2,y1,y2,fx1y1,fx2y1,fx1y2,fx2y2,FL,FV;
	int i_l,i_u,j_l,j_u;

	//--------Scanning Table to find local grid

	// scan through mu
	for(int j = 0; j < Mu_N; j++){
		if(logspace(Mu_a,Mu_b,Mu_N,j) >= mu_draw){
			j_l = j-1;
			j_u = j;
			break;
		}
	}

	// scan through F
	for(int i = 0; i < F_N; i++){
		if(linspace(F_a,F_b,F_N,i) >= F_draw){
			i_l = i-1;
			i_u = i;
			break;
		}
	}

	// assigning corners (for both lambda and nu)
	x1 = logspace(Mu_a,Mu_b,Mu_N,j_l);
	x2 = logspace(Mu_a,Mu_b,Mu_N,j_u);
	y1 = linspace(F_a,F_b,F_N,i_l);
	y2 = linspace(F_a,F_b,F_N,i_u);

	// warning if corners fucked up
	if(x1 > x2 || y1 > y2){cout<<"_____Warning! BiTerp Corners are Wrong!_____"<<endl;}

	//--------Getting Lamba

	// corner heights
	fx1y1 = L_Table[(int)j_l][(int)i_l];
	fx1y2 = L_Table[(int)j_l][(int)i_u];
	fx2y1 = L_Table[(int)j_u][(int)i_l];
	fx2y2 = L_Table[(int)j_u][(int)i_u];

	// calculating
	FL = (1./((x2-x1)*(y2-y1)))*((fx1y1*(x2-x)*(y2-y)) + (fx2y1*(x-x1)*(y2-y)) + (fx1y2*(x2-x)*(y-y1)) + (fx2y2*(x-x1)*(y-y1)));

	//--------Getting Nu

	// corner heights
	fx1y1 = V_Table[(int)j_l][(int)i_l];
	fx1y2 = V_Table[(int)j_l][(int)i_u];
	fx2y1 = V_Table[(int)j_u][(int)i_l];
	fx2y2 = V_Table[(int)j_u][(int)i_u];

	// calculating
	FV = (1./((x2-x1)*(y2-y1)))*((fx1y1*(x2-x)*(y2-y)) + (fx2y1*(x-x1)*(y2-y)) + (fx1y2*(x2-x)*(y-y1)) + (fx2y2*(x-x1)*(y-y1)));

	// store results
	G_l = TMath::Power(10.,FL);
	G_v = FV;

	// Done! :)
}

//-----------------Zombie_Fix------------------
// Random walk (hence zombie) algorithm to find l
// and nu for difficult points
//
// INPUTS:	mu,F
// 			tol = tolerance
// 			bool verbose print out option
//
// OUTPUT:	G_l and G_v stored globally
//----------------------------------------------
void Zombie_Fix(double mu, double F, double tol, bool verbose){

	// Declare variables
	double step_l, step_v, l, v, theta, l_c, v_c, p, p_max, boost;
	int p_max_a;
	double l_circle[8], v_circle[8];

	// Initial guess and steps
	LV_asymp(mu,F);
	Gerror(l_asymp,v_asymp,mu,F);
	l = TMath::Log10(l_asymp);
	v = v_asymp;
	step_v = 0.01*TMath::Sqrt(TMath::Power(G_mu_e,2.) + TMath::Power(G_F_e,2.));
	step_l = step_v;

	// create circle
	double ang[8] = {0.,TMath::Pi()/4.,TMath::Pi()/2.,3.*TMath::Pi()/4.,TMath::Pi(),5.*TMath::Pi()/4.,3.*TMath::Pi()/2.,7.*TMath::Pi()/4.};

	// start counter
	int iter = 0;

	// while loop when error too big
	while(G_F_e > tol || G_mu_e > tol || iter < 1000){

		// update counter
		iter += 1;

		// Draw random angle
		boost = gen->Uniform(0.,2.*TMath::Pi());

		// Reset
		p_max = 0.;
		p_max_a = 0;

		// loop around circle
		for(int a = 0; a < 8; a++){

			// define angle
			theta = ang[a] + boost;

			// calculate and store l and nu
			l_c = l + (step_l*TMath::Cos(theta));
			v_c = v + (step_v*TMath::Sin(theta));
			l_circle[a] = l_c;
			v_circle[a] = v_c;

			// assess error
			Gerror(TMath::Power(10.,l_c),v_c,mu,F);
			p = 1./TMath::Sqrt(TMath::Power(G_mu_e,2.) + TMath::Power(G_F_e,2.));

			// find maximum (most accurate point)
			if(p > p_max){
				p_max = p;
				p_max_a = a;
			}
		}

		// pick l and nu
		l = l_circle[p_max_a];
		v = v_circle[p_max_a];

		// assess error, define step size
		Gerror(TMath::Power(10.,l),v,mu,F);
		step_v = 0.01*TMath::Sqrt(TMath::Power(G_mu_e,2.) + TMath::Power(G_F_e,2.));
		step_l = step_v;

		// exit if too many iterations
		if(iter > 1000000 && verbose == true){
			cout<<"Warning! I had to escape! mu = "<<mu<<" F = "<<F<<endl;
			break;
		}
	}

	// set results!
	G_l = TMath::Power(10.,l);
	G_v = v;

	// optiona printout
	if(verbose == true){
		cout<<" "<<endl;
		cout<<"                        Zombie_Fix Results:"<<endl;
		cout<<"   ------------------------------------------------------------     "<<endl;
		cout<<"     COM Parameters: lambda = "<<G_l<<", nu = "<<G_v<<endl;
		cout<<"     Results: mean = "<<G_mu<<", F = "<<(G_var/G_mu)<<endl;
		cout<<"     Error: mean_error = "<<G_mu_e*100.<<"%, F_error = "<<G_F_e*100.<<"%"<<endl;
		cout<<"   ------------------------------------------------------------     "<<endl;
		cout<<" "<<endl;
	}

	// done! :)
}

//--------------------Z-------------------------
// compute Z of COM-Poisson distribution
//
// INPUTS: 	l, v
//
// OUTPUT:	Z
//----------------------------------------------
Double_t Z(double l, double v){

	// initialize
	double s = 0.;
	double termLast = 0.;
	double term = 1.;
	double tol = 0.00000001;
	double p;

	// compute sum
	do{	s = s + 1.;

		// escape if too many iterations
		if(s > 10000){
			cout<<"I CAN'T FIND Z! >:("<<endl;
			break;
		}
		termLast = term;

		// compute exponent of next sum term, with option to use Stirling approximation
		// if s large
		if(s > 170.){
			p = (s*TMath::Log(l)) - (v*Stirling(s));

		}else{p = (s*TMath::Log(l)) - (v*TMath::Log(TMath::Factorial(s)));}

		// add term to sum
		term = term + TMath::Exp(p);

	// convergence condition
	}while(TMath::Abs(termLast - term) > (tol*term));

	// give result
	Double_t Z = term;
	return Z;
}

//--------------------Z_asymp-----------------------
// asymptotic formula for high Z
//
// INPUTS: 	l, v
//
// OUTPUT:	Z
//----------------------------------------------
Double_t Z_asymp(double l, double v){

	double logZ = (v*TMath::Power(l,(1./v))) - (((v-1.)/(2.*v))*TMath::Log(l)) - (((v-1.)/2.)*TMath::Log(2.*TMath::Pi())) - (0.5*TMath::Log(v));

	return logZ;
}

//--------------------logZ----------------------
// Function to return the log of Z using either
// Z or Z_asymp
// INPUTS: 	l, v
//
// OUTPUT:	Z
//----------------------------------------------
Double_t logZ(double l, double v){

	// Try using Z
	double logZ = TMath::Log(Z(l,v));

	// asympotic formula if that didn't work
	if(logZ == (1./0.)){logZ = Z_asymp(l,v);}

	return logZ;
}

//--------------------COM-----------------------
// COM-Poisson PDF, to be turned into TF1
//
// INPUTS: 	palpha[0] = x
// 			par[0] = l
// 			par[1] = v
//
// OUTPUT:	P
//----------------------------------------------
Double_t COM(Double_t *palpha, Double_t *par){

	// initialize
	Double_t x = (palpha[0]);
	Double_t l = par[0];
	Double_t v = par[1];
	Double_t P,p;

	// calculate Z
	Double_t logz = logZ(l,v);

	// calculate probability (with option for Stilring approximation)
	if(x > 100){
		p = (x*TMath::Log(l)) - (v*Stirling(x)) - logz;
	}else{p = (x*TMath::Log(l)) - (v*TMath::Log(TMath::Factorial(x))) - logz;}
	P = TMath::Exp(p);
	return P;
}

//------------------Moments---------------------
// calculates mean and variance for given l,v
//
// INPUTS: 	l, v
//
// OUTPUT:	G_mu and G_var stored globally
//----------------------------------------------
void Moments(double l, double v){

	//-----Mean----------------------

	// initializing
	double s = 0.;
	double termLast = 1000.;
	double term = 0.;
	double tol = 0.0000001;		// tolerance
	double p;

	// compute sum
	do{	s = s + 1.;

		// escape if too many iterations
		if(s > 10000){
			cout<<"I CAN'T FIND mu! >:("<<endl;
			break;
		}

		// updating last term
		termLast = term;

		// compute exponent of next sum term, with option to use Stirling approximation
		// if s large
		if(s > 170){
			p = TMath::Log(s) + (s*TMath::Log(l)) - logZ(l,v) - (v*Stirling(s));
		}else{p = TMath::Log(s) + (s*TMath::Log(l)) - logZ(l,v) - (v*(TMath::Log(TMath::Factorial(s))));}

		// add term to sum
		if(p < -700.){
			term = term + 0.;
		}else{term = term + TMath::Exp(p);}

	// convergence condition
	}while(TMath::Abs(termLast - term) > (tol*term));

	// store result
	G_mu = term;

	//-----Variance----------------------

	// initalizing
	s = 0.;
	termLast = 1000.;
	term = 0.;
	tol = 0.00000001;		// tolerance

	// compute sum
	do{	s = s + 1.;

		// escape if too many iterations
		if(s > 10000){
			cout<<"I CAN'T FIND var! >:("<<endl;
			break;
		}

		// updating last term
		termLast = term;

		// compute exponent of next sum term, with option to use Stirling approximation
		// if s large
		if(s > 170){
			p = (2.*TMath::Log(s)) + (s*TMath::Log(l)) - logZ(l,v) - (v*Stirling(s));
		}else{p = (2.*TMath::Log(s)) + (s*TMath::Log(l)) - (v*TMath::Log(TMath::Factorial(s))) - logZ(l,v);}

		// add term to sum
		if(p < -700.){
			term = term + 0.;
		}else{term = term + TMath::Exp(p);}

	// convergence condition
	}while(TMath::Abs(termLast - term) > (tol*term));

	// storing result
	G_var = term - TMath::Power(G_mu,2.);

	// Catch for asymptotic
	if(G_var <= 0. || G_mu <= 0.){Moments_Asymp(l,v);}

	// Set G_F
	G_F = G_var/G_mu;

	// Done! :)
}

//--------------------Gerror---------------------
// simple function to calculate error
//
// INPUTS: 	l,v,mu_want,F_want
//
// OUTPUT:	G_mu_e,G_var_e,G_F_e stored globally
//----------------------------------------------
void Gerror(double l, double v, double mu_want, double F_want){

	double var_want = mu_want*F_want;

	// calcuate error
	Moments(l,v);
	G_mu_e = TMath::Abs((G_mu - mu_want)/mu_want);
	G_var_e = TMath::Abs((G_var - var_want)/var_want);
	G_F_e = TMath::Abs((G_F - F_want)/F_want);
}

//-----------------Moments_Asymp---------------------
// Calculates moments using asymptotic expressions
//
// INPUTS:	l, v
//
// OUTPUT:	G_mu, G_var, G_F stored globally
//---------------------------------------------------
void Moments_Asymp(double l, double v){

	// Output asymptotic expressions globally
	G_mu = TMath::Power(l,(1./v)) - ((v - 1.)/(2.*v));
	G_var = (1./v)*TMath::Power(l,(1./v));
	G_F = G_var/G_mu;
}

//-----------------isBernoulli------------------------
// Function to check if a given mu,F is forbidden by
// the 1st, 2nd, or 3rd binomial mode hump
//
// INPUTS:	mu, F
//
// OUTPUT:	bool isBinom (true if it is forbidden)
//---------------------------------------------------
bool isBernoulli(double mu, double F){

	// initialize
	double var = mu*F;
	bool isBinom = false;

	// check for 1st limiting case
	double b1 = -(mu)*(mu - 1.);
	if(mu >= 0. && mu <= 1. && var <= b1){isBinom = true;}

	// check for 2nd limiting case
	double b2 = -(mu - 1.)*(mu - 2.);
	if(mu >= 1. && mu <= 2. && var <= b2){isBinom = true;}

	// check for 3rd limiting case
	double b3 = -(mu - 2.)*(mu - 3.);
	if(mu >= 2. && mu <= 3. && var <= b3){isBinom = true;}

	return isBinom;
}

//------------------LV_Asymp--------------------
// Makes asymptotic approximations for l and v
//
// INPUTS: 	mu = mean wanted
// 			F = fano factor wanted
// OUTPUT:	l_asymp and v_asymp stored globally
//----------------------------------------------
void LV_asymp(double mu, double F){
	v_asymp = (2.*mu + 1. + TMath::Sqrt(4.*mu*mu + 4.*mu + 1. - 8.*mu*F))/(4.*mu*F);
	l_asymp = TMath::Power((v_asymp*mu*F),v_asymp);
}

//--------------------isSoft--------------------
// Function to check if something falls in the fuzzy
// edge of a bump, fuzz thickness chosen by you!
//
// INPUTS: 	mu_want, F_want
// 			tol = fuzz depth
//
// OUTPUT:	bool, true == in the fuzz
//----------------------------------------------
bool isSoft(double mu, double F, double tol){

	// initialize
	bool soft = false;

	// check 7 different conditions
	if(isBernoulli((mu - (tol*mu)),(F - (tol*F))) == true){soft = true;}
	if(isBernoulli((mu - (tol*mu)),(F + (tol*F))) == true){soft = true;}
	if(isBernoulli((mu + (tol*mu)),(F - (tol*F))) == true){soft = true;}
	if(isBernoulli((mu + (tol*mu)),(F + (tol*F))) == true){soft = true;}
	if(isBernoulli((mu),(F - (tol*F))) == true){soft = true;}
	if(isBernoulli((mu - (0.5*tol*mu)),(F - (tol*F))) == true){soft = true;}
	if(isBernoulli((mu + (0.5*tol*mu)),(F - (tol*F))) == true){soft = true;}

	// Done! :)
	return soft;
}

//----------------cdfBuild----------------------
// builds cdf of COM distribution with "smart"
// beginning and end points
//
// INPUTS: 	None (set globally in main)
// 			Things needed: mu,variance, and
// 			for f_COM to be set up
//
// OUTPUT:	cdf stored globally in std vec "cdf"
//----------------------------------------------
void cdfBuild(){

	// clearing vector
	freeFromMemory(cdf);

	// defining smart starting and end points
	double numS = 5.;
	if(G_var < 1.){numS = 10.;}
	cdf_a = (int)TMath::Max(0.,(G_mu - (numS*G_var)));
	cdf_b = (int)TMath::Max((G_mu + (G_var*numS)),5.);
	cdf_n = cdf_b - cdf_a + 1;

	// creating cdf
	cdf.push_back(f_COM->Eval(cdf_a));
	double pdf;
	for(int i = 1; i < cdf_n; i++){
		pdf = f_COM->Eval(i+cdf_a);
		cdf.push_back(cdf[i-1] + pdf);
	}
}

//----------------cdfBuild_FAST-----------------
// builds cdf of COM distribution with "smart"
// beginning and end points, fast iterative method
//
// INPUTS: 	None (set globally in main)
// 			Things needed: mu,variance, and
// 			for f_COM to be set up
//
// OUTPUT:	cdf stored globally in std vec "cdf"
//----------------------------------------------
void cdfBuild_FAST(){

	// clearing vector
	freeFromMemory(cdf);

	// defining smart starting and end points
	double numS = 5.;
	if(G_var < 1.){numS = 10.;}
	cdf_a = (int)TMath::Max(0.,(G_mu - (numS*G_var)));
	cdf_b = (int)TMath::Max((G_mu + (G_var*numS)),5.);
	cdf_n = cdf_b - cdf_a + 1;

	// starting with initial point
	double P0 = f_COM->Eval(cdf_a);
	cdf.push_back(P0);
	double pdf1,pdf0;
	pdf0 = P0;

	// creating cdf
	for(int i = 1; i < cdf_n; i++){
		pdf1 = (G_l/TMath::Power(i+cdf_a,G_v)*pdf0);
		pdf0 = pdf1;
		cdf.push_back(cdf[i-1] + pdf1);
	}
}

//----------------cdfBernoulli------------------
// Bernoulli distribution cdf
//
// INPUTS: 	None (set globally in main)
// 			Things needed: mu,variance, and
// 			for f_COM to be set up
//
// OUTPUT:	cdf stored globally in std vec "cdf"
//----------------------------------------------
void cdfBernoulli(){

	// clearing vector
	freeFromMemory(cdf);

	// defining smart starting and end points
	cdf_a = (int)TMath::Max(0.,floor(G_mu - 2));
	cdf_b = (int)floor(G_mu + 2);
	cdf_n = cdf_b - cdf_a + 1;

	// distribution parameters
	double p = G_mu - floor(G_mu);
	double q = 1. - p;

	// creating cdf
	for(int i = 0; i < cdf_n; i++){
		if(i <= (int)floor(G_mu - 1)){cdf.push_back(0);}
		if(i == (int)floor(G_mu)){cdf.push_back(q);}
		if(i >= (int)floor(G_mu + 1)){cdf.push_back(1);}
	}
}

//----------------pdfBernoulli------------------
// Bernoulli distribution pdf
//
// INPUTS: 	N, mu
//
// OUTPUT:	returns value of PDF
//----------------------------------------------
double pdfBernoulli(double N, double mu){

	//distribution parameters
	double p = mu - floor(mu);
	double q = 1. - p;

	//creating cdf
	double P;
	if(N <= (int)floor(mu - 1)){P = 0.;}
	if(N == (int)floor(mu)){P = q;}
	if(N == (int)floor(mu + 1)){P = p;}
	if(N > (int)floor(mu + 1)){P = 0.;}

	return P;
}

//----------------cdfGauss----------------------
// Gauss distribution cdf
//
// INPUTS: 	None (set globally in main)
// 			Things needed: mu,variance, and
// 			for f_COM to be set up
//
// OUTPUT:	cdf stored globally in std vec "cdf"
//----------------------------------------------
void cdfGauss(){

	// clearing vector
	freeFromMemory(cdf);

	// defining smart starting and end points
	double numS = 5.;
	if(G_var < 1.){numS = 10.;}
	cdf_a = (int)TMath::Max(0.,(G_mu - (numS*G_var)));
	cdf_b = (int)TMath::Max((G_mu + (G_var*numS)),5.);
	cdf_n = cdf_b - cdf_a + 1;
	double STD = TMath::Sqrt(G_var);

	// warning
	if(G_mu < G_Transition){cout<<"Warning! Inappropriate use of cdfGauss!"<<endl;}

	cdf.push_back(TMath::Gaus(cdf_a,G_mu,STD,kTRUE));
	double pdf;
	for(int i = 1; i < cdf_n; i++){
		pdf = TMath::Gaus((i+cdf_a),G_mu,STD,kTRUE);
		cdf.push_back(cdf[i-1] + pdf);
	}
}

//----------------Bernoulli---------------------
// calculates Bernoulli variance
//
// INPUTS: 	mu = mean
//
// OUTPUT:	variance of Bernoulli distribution
//----------------------------------------------
double Bernoulli(double mu){

	double l,u;
	l = floor(mu);
	u = l+1.;

	double var = -(mu - u)*(mu - l);

	return var;
}

//-----------------linspace--------------------------
// gives linearly spaced values
// INPUTS: 	a = starting point
// 			b = end point
// 			n = size of series
// 			i = position in series
//
// OUTPUT:	x
//---------------------------------------------------
double linspace(double a, double b, double n, double i){
	double w = (b - a)/(n-1.);
	double x = a + (w*i);
	return x;
}

//-----------------logspace:---------------------
//
// General code to produce log scaled values
//
// INPUTS: 	a = lowest desired WIMP Mass (GeV)
// 			b = highest desired WIMP Mass (GeV)
// 			n = number of desired calculations (log spaced)
// 			i = position in range of values desired
//
// OUTPUT: 	Mass = log-scaled mass desired (GeV)
//
//---------------------------------------------------
double logspace(double a, double b, double n, double i){
	// calculating interval for log-spaced masses
	double gap = (TMath::Log10(b) - TMath::Log10(a))/(n - 1.);

	// calculation of mass, addition to list, optional printring
	double Mass = TMath::Power(10,(TMath::Log10(a) + (i*gap)));

	// Done! :)
	return Mass;
}

//-----------------Stirling--------------------------
// Stirling's approximation
//
// INPUTS: 	integer n
//
// OUTPUT: 	Log(n!)
//---------------------------------------------------
double Stirling(int n){
	double LNF = TMath::Log(TMath::Sqrt(2.*TMath::Pi()*n)) + (n*TMath::Log(n)) - n;
	return LNF;
}

//-----------------BinLogX:--------------------------
// Code to have uniform bins in log-x space for histogram
//
// INPUTS: 	pointer of histogram
//
// OUTPUT: 	updates plot
//---------------------------------------------------
void BinLogX(TH1*h){
   TAxis *axis = h->GetXaxis();
   int bins = axis->GetNbins();
   Axis_t from = axis->GetXmin();
   Axis_t to = axis->GetXmax();
   Axis_t width = (to - from) / bins;
   Axis_t *new_bins = new Axis_t[bins + 1];
   for (int i = 0; i <= bins; i++) {new_bins[i] = TMath::Power(10, from + i * width);}
   axis->Set(bins, new_bins);
   delete new_bins;
}

//-----------------------------------------------------------------------------------------

#endif // COMPOISSON_H_INCLUDED
