#ifndef SIMUPRIMARYELECTRONS_H_INCLUDED
#define SIMUPRIMARYELECTRONS_H_INCLUDED

//C++
#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <chrono>

//ROOT
#include "TROOT.h"
#include "TFile.h"
#include "TMath.h"
#include "TTree.h"
#include "TF1.h"
#include "TF3.h"

#include "TCanvas.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TApplication.h"
#include "TRandom3.h"


//Local
#include "MCfunctions1.h"

#endif //SIMUPRIMARYELECTRONS_H_INCLUDED
